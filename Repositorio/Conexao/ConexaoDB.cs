﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Conexao
{
    public static class ConexaoDB
    {
        public static SqlConnection Conectar()
        {
            var conexao = ConfigurationManager.ConnectionStrings["DesavioBD"].ConnectionString;
            var objetoConexao = new SqlConnection(conexao);
            objetoConexao.Open();
            var ObjetoCommand = new SqlCommand
            {
                Connection = objetoConexao
            };

            return ObjetoCommand.Connection;
        }
    }
}
